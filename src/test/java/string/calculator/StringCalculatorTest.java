package string.calculator;

import org.junit.Before;
import org.junit.Test;
import string.calculator.exception.NegativesNotAllowedException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class StringCalculatorTest {

    private StringCalculator stringCalculator;

    @Before
    public void setUp() {
        stringCalculator = new StringCalculatorImpl();
    }

    @Test
    public void should_ReturnZero_ForEmptyString() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("");

        //then
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void should_ReturnFirstArgument_ForOneArgumentString() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("1");

        //then
        assertThat(result).isEqualTo(1);
    }

    @Test
    public void should_ReturnSum_ForTwoArgumentsString() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("1,2");

        //then
        assertThat(result).isEqualTo(3);
    }

    @Test
    public void should_ReturnSum_ForManyArgumentsString() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("1,2,3,4,5");

        //then
        assertThat(result).isEqualTo(15);
    }

    @Test
    public void should_ReturnSum_ForCommaAndNewLineDelimeters() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("1,2,3,4\n5");

        //then
        assertThat(result).isEqualTo(15);
    }

    @Test
    public void should_AddNewDelimiter_ForNewDelimeterPrefix() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("//:\n1:2:3:4\n5");

        //then
        assertThat(result).isEqualTo(15);
    }

    @Test
    public void should_ThrowException_ForNegativeNumbers() {

        //given
        //when
        final Throwable raisedException = catchThrowable(() -> stringCalculator.add("1,2,-3,-4,-5"));

        //then
        assertThat(raisedException).isInstanceOf(NegativesNotAllowedException.class)
                .hasMessageContaining("-3")
                .hasMessageContaining("-4")
                .hasMessageContaining("-5");
    }

    @Test
    public void should_ReturnCorrectedSum_ForNumbersLargerThan1000ThatShouldBeIgnored() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("1,2,1001,3004\n5");

        //then
        assertThat(result).isEqualTo(8);
    }

    @Test
    public void should_AddNewDelimiter_ForNewDelimiterLongerThanOnePrefix() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("//[***]\n1***2***3***4***5");

        //then
        assertThat(result).isEqualTo(15);
    }

    @Test
    public void should_AddNewDelimiters_ForNewMultipleDelimitersLongerThanOnePrefix() throws NegativesNotAllowedException {

        //given
        //when
        int result = stringCalculator.add("//[***][%%%]\n1***2%%%3***4\n5");

        //then
        assertThat(result).isEqualTo(15);
    }
}