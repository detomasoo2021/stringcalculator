package string.calculator;

import string.calculator.exception.NegativesNotAllowedException;

public interface StringCalculator {

    /**
     * Add numbers from given number list.
     * @param numbers numbers to be added, can contain 0, 1 or 2 arguments
     * @return sum of the arguments (0 for empty string_
     * @throws NegativesNotAllowedException if negative value found
     */
    int add(String numbers) throws NegativesNotAllowedException;
}
