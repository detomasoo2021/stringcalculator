package string.calculator;

import string.calculator.exception.NegativesNotAllowedException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringCalculatorImpl implements StringCalculator {

    private static final String NEWLINE = "\n";
    private static final String ALLOWED_DELIMITER = ",";
    private static final String NEW_DELIMITER_PREFIX = "//";
    private static final String NEW_DELIMITER_SUFFIX = NEWLINE;
    private static final String NEW_DELIMITERS_WITH_BRACKETS_REGEX = "(\\[.+\\])+";
    private static final String NEW_DELIMITER_WITH_BRACKETS_SYNTAX = NEW_DELIMITER_PREFIX + NEW_DELIMITERS_WITH_BRACKETS_REGEX + NEW_DELIMITER_SUFFIX;
    private static final String NEW_DELIMITER_WITHOUT_BRACKETS_REGEX = ".+";
    private static final String NEW_DELIMITER_SYNTAX = NEW_DELIMITER_PREFIX + "(" + NEW_DELIMITERS_WITH_BRACKETS_REGEX + "|" + NEW_DELIMITER_WITHOUT_BRACKETS_REGEX + ")" + NEW_DELIMITER_SUFFIX;
    private static final String EMPTY_STRING = "";
    private static final String NEGATIVES_NOT_ALLOWED_EXCEPTION_MESSAGE_PREFIX = "Negatives not allowed. Found negative numbers: ";
    private static final String NEW_DELIMITERS_WITHIN_BRACKETS_SPLIT_REGEX = "\\]\\[";
    private static final String DELIMITERS_WITH_BRACKETS_PREFIX = "[";

    @Override
    public int add(String numbers) throws NegativesNotAllowedException {
        if (numbers == null || numbers.isEmpty()) {
            return 0;
        }

        Optional<List<String>> optionalDelimiters = getOptionalDelimiters(numbers);
        String allowedDelimiters = generateAllowedDelimiters(optionalDelimiters);
        List<Integer> numberList = getNumberOnlyList(numbers, allowedDelimiters);
        checkIfThereAreNegatives(numberList);
        return numberList.stream()
                .filter(number -> number <= 1000)
                .reduce(0, Integer::sum);
    }

    private void checkIfThereAreNegatives(List<Integer> numberList) throws NegativesNotAllowedException {

        List<Integer> negativeNumberList = numberList.stream()
                .filter(number -> number < 0)
                .collect(Collectors.toList());
        if (!negativeNumberList.isEmpty()) {
            StringBuilder exceptionMessageBuilder = new StringBuilder()
                    .append(NEGATIVES_NOT_ALLOWED_EXCEPTION_MESSAGE_PREFIX);
            negativeNumberList.stream()
                    .forEachOrdered(number -> exceptionMessageBuilder.append(number)
                            .append(", "));
            int length = exceptionMessageBuilder.length();
            String message = exceptionMessageBuilder
                    .replace(length - 2, length, ".")
                    .toString();
            throw new NegativesNotAllowedException(message);

        }

    }

    private List<Integer> getNumberOnlyList(String numbers, String allowedDelimiters) {

        return Arrays.stream(
                numbers.replaceFirst(NEW_DELIMITER_SYNTAX, EMPTY_STRING).split(allowedDelimiters))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    private String generateAllowedDelimiters(Optional<List<String>> optionalDelimiters) {
        StringBuilder allowedDelimiterRegexBuilder = new StringBuilder();
        optionalDelimiters.ifPresentOrElse(delimiters -> {
                    delimiters.forEach(delimiter -> {
                        allowedDelimiterRegexBuilder
                                .append(Pattern.quote(delimiter))
                                .append("|");
                    });
                    allowedDelimiterRegexBuilder.deleteCharAt(allowedDelimiterRegexBuilder.lastIndexOf("|"));
                },
                () -> allowedDelimiterRegexBuilder.append(ALLOWED_DELIMITER));
        return allowedDelimiterRegexBuilder
                .append("|")
                .append(NEWLINE)
                .toString();
    }

    private Optional<List<String>> getOptionalDelimiters(String numbers) {

        Matcher matcher = getMatcherForNewDelimiterSyntax(numbers);
        boolean matchFound = matcher.find();
        if (!matchFound) {
            return Optional.empty();
        }

        String delimiters = getDelimiters(matcher.group());

        if (delimiters.startsWith(DELIMITERS_WITH_BRACKETS_PREFIX)) {
            return getDelimitersInBrackets(delimiters);
        } else {
            return getDelimiterWithoutBrackets(delimiters);
        }
    }

    private Optional<List<String>> getDelimiterWithoutBrackets(String delimiters) {
        return Optional.of(List.of(delimiters));
    }

    private Optional<List<String>> getDelimitersInBrackets(String delimitersWithBrackets) {

        Pattern pattern = Pattern.compile(NEW_DELIMITERS_WITH_BRACKETS_REGEX);
        Matcher matcher = pattern.matcher(delimitersWithBrackets);
        boolean matchFound = matcher.find();

        if (!matchFound) {
            return Optional.empty();
        }

        return Optional.of(Arrays.asList(delimitersWithBrackets
                .substring(1, delimitersWithBrackets.length() - 1)
                .split(NEW_DELIMITERS_WITHIN_BRACKETS_SPLIT_REGEX)));
    }

    private String getDelimiters(String delimiterGroup) {

        int beginIndex = NEW_DELIMITER_PREFIX.length();
        int endIndex = delimiterGroup.length() - NEW_DELIMITER_SUFFIX.length();
        return delimiterGroup.substring(beginIndex, endIndex);
    }

    private Matcher getMatcherForNewDelimiterSyntax(String numbers) {
        Pattern pattern = Pattern.compile(NEW_DELIMITER_SYNTAX);
        return pattern.matcher(numbers);
    }
}
